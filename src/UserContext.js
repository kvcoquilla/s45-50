import React from 'react'


//A context is a special React object which will allow us to store information within and pass it around our components within the app.

// The context object is a different approach to 
const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;
